create table users (
	id serial primary key,
	username varchar(20) not null,
	password varchar(100) not null,
	first_name varchar(30),
	last_name varchar(30)
);
CREATE UNIQUE INDEX idx_users_username ON users(username);


insert into users (username,password,first_name, last_name) values('demo','$2b$10$hcRi0nP0iHQupPe2a2lJzOHZ3/2kdtiJs8hShvPZdNqK44aXsAP3q','USER','DEMO');
insert into users (username,password,first_name, last_name) values('taspen','$2b$10$hcRi0nP0iHQupPe2a2lJzOHZ3/2kdtiJs8hShvPZdNqK44aXsAP3q','USER','TASPEN');


create table peserta (
	id serial primary key,
	notas varchar(30),
	nama varchar(100),
	nik varchar(30),
	alamat TEXT
);
CREATE UNIQUE INDEX idx_peserta_notas ON peserta(notas);

create table enrollment (
	id serial primary key,
	username varchar(20) not null,
	notas varchar(30),
	photo bytea,
	biometric_response JSONB,
	biometric_status varchar(10),
	flag CHAR(1),
	latitude varchar(20),
	longitude varchar(20),
	date timestamp,
	CONSTRAINT fk_enrollment_username FOREIGN KEY(username) REFERENCES users(username),
	CONSTRAINT fk_enrollment_notas FOREIGN KEY(notas) REFERENCES peserta(notas)
);

create table otentikasi (
	id serial primary key,
	username varchar(20) not null,
	notas varchar(30),
	photo bytea,
	biometric_response JSONB,
	biometric_status varchar(10),
    ned_response JSONB,
	ned_status varchar(10),
	latitude varchar(20),
	longitude varchar(20),
	date timestamp,
	CONSTRAINT fk_otentikasi_username FOREIGN KEY(username) REFERENCES users(username),
	CONSTRAINT fk_otentikasi_notas FOREIGN KEY(notas) REFERENCES peserta(notas)
);


create table ocr_ktp (
	id serial primary key,
	username varchar(20) not null,
	reference_id varchar(100),
	photo bytea,
	biometric_response JSONB,
	biometric_status varchar(10),    
	date timestamp,
	CONSTRAINT fk_otentikasi_username FOREIGN KEY(username) REFERENCES users(username)
);


create table ecertificate_verification (
	id serial primary key,
	username varchar(20) not null,
	name varchar(100),
	birth_date date,
	email varchar(100),
	phone varchar(100),
	selfie_photo bytea,
	ktp_photo bytea,
	channel varchar(100),
	reference_id varchar(100),
	biometric_response JSONB,
	biometric_status varchar(10),
	CONSTRAINT fk_ecertificate_verification_username FOREIGN KEY(username) REFERENCES users(username)
);

CREATE TABLE pmt_channel_progress (
	id serial primary key,
	otentikasi_id serial NOT NULL,
	bank_code varchar(5) NOT NULL,
	"timestamp" timestamp NOT NULL,
	retry_no int4 NOT NULL,
	status varchar(10) NOT NULL,
	CONSTRAINT fk_pmt_channel_progress_otentikasi_id FOREIGN KEY (otentikasi_id) REFERENCES otentikasi(id)
);