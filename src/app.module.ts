import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './user/user.controller';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { UserService } from './user/user.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user/user.entity';
import { VerihubsModule } from './verihubs/verihubs.module';
import { NedModule } from './ned/ned.module';
import { DataSource } from 'typeorm';
import { Peserta } from './entity/peserta.entity';
import { Enrollment } from './entity/enrollment.entity';
import { Otentikasi } from './entity/otentikasi.entity';
import { Ocrktp } from './entity/ocrktp.entity';
import { Ecertificate } from './entity/ecertificate.entity';
import { SimulationModule } from './simulation/simulation.module';
import { PmtChannelProgressEntity } from './entity/pmtChannelProgress.entity';

@Module({
  imports: [
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1d' },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '10.10.105.248',
      port: 5432,
      password: 'password',
      username: 'taspen',
      entities: [User, Peserta, Enrollment, Otentikasi, Ocrktp, Ecertificate, PmtChannelProgressEntity],
      database: 'taspen',
      synchronize: false,
      logging: true,
    }),
    TypeOrmModule.forFeature([Peserta, Enrollment, Otentikasi, Ocrktp, Ecertificate]),
    UserModule,
    VerihubsModule,
    NedModule,
    SimulationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
