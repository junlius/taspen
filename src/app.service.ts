import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Peserta } from './entity/peserta.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';
import { FaceEnrollmentDto } from './verihubs/dto/face.enrollment.dto';
import { Enrollment } from './entity/enrollment.entity';
import { Otentikasi } from './entity/otentikasi.entity';
import { FaceVerificationDto } from './verihubs/dto/face.verification.dto';
import { KtpExtractDto } from './verihubs/dto/ktp.extract.dto';
import { Ocrktp } from './entity/ocrktp.entity';
import { ElectronicCertificateVerificationDto } from './verihubs/dto/ecertificate.verification.dto';
import { Ecertificate } from './entity/ecertificate.entity';
import { FilterOperator, FilterSuffix, Paginate, PaginateQuery, paginate, Paginated } from 'nestjs-paginate';

@Injectable()
export class AppService {
  constructor(private readonly jwtService: JwtService,
              @InjectRepository(Peserta) private readonly pesertaRepository: Repository<Peserta>,
              @InjectRepository(Enrollment) private readonly enrollmentRepository: Repository<Enrollment>,
              @InjectRepository(Otentikasi) private readonly otentikasiRepository: Repository<Otentikasi>,
              @InjectRepository(Ocrktp) private readonly ocrktpRepository: Repository<Ocrktp>,
              @InjectRepository(Ecertificate) private readonly ecertificateRepository: Repository<Ecertificate>) {}

  getHello(): string {
    return 'Hello World!';
  }

  getJWTUser(authorization: string) {
    let user = 'unknown';

    try {
      const [type, token] = authorization?.split(' ') ?? [];
      const json = this.jwtService.decode(token, { json: true }) as { sub: string };
      user = json.sub;
    } catch {}


    return user;
  }

  viewPeserta(notas: string): Promise<Peserta> {
    return this.pesertaRepository.findOneBy({ notas });
  }

  async saveEnrollment(authorization: string, data: FaceEnrollmentDto, result: any, status: string) {   
    try {
      const username = this.getJWTUser(authorization);      
      if (!await this.viewPeserta(data.subject_id)) {
        let peserta = new Peserta();
        peserta.notas = data.subject_id;
        this.pesertaRepository.save(peserta);
      }

      let enrollment = new Enrollment();
      enrollment.username = username;
      enrollment.notas = data.subject_id;
      if (data.image) {
        enrollment.photo = data.image;
      }
      enrollment.biometricResponse = result;
      enrollment.biometricStatus = status;
      enrollment.latitude = data.latitude;
      enrollment.longitude = data.longitude;      
      enrollment.date = new Date();
      this.enrollmentRepository.save(enrollment);
      

    } catch {}    
  }

  async saveOcrktp(authorization: string, data: KtpExtractDto, result: any, status: string) {   
    try {
      const username = this.getJWTUser(authorization);      
      // if (!await this.viewPeserta(data.subject_id)) {
      //   let peserta = new Peserta();
      //   peserta.notas = data.subject_id;
      //   this.pesertaRepository.save(peserta);
      // }

      let ocrKtp = new Ocrktp();
      ocrKtp.username = username;
      console.log(ocrKtp.username);
      ocrKtp.referenceId = data.reference_id;
      if (data.image) {
        ocrKtp.photo = data.image;
      }
      ocrKtp.biometricResponse = result;
      ocrKtp.biometricStatus = status;      
      ocrKtp.date = new Date();
      this.ocrktpRepository.save(ocrKtp);
      

    } catch {}    
  }

  async saveEcertificateVerification(authorization: string, data: ElectronicCertificateVerificationDto, result: any, status: string) {   
    try {
      const username = this.getJWTUser(authorization);      
      // if (!await this.viewPeserta(data.subject_id)) {
      //   let peserta = new Peserta();
      //   peserta.notas = data.subject_id;
      //   this.pesertaRepository.save(peserta);
      // }

      let ecertificate = new Ecertificate();
      ecertificate.username = username;
      console.log(ecertificate.username);
      ecertificate.name = data.name;
      ecertificate.birthDate = data.birth_date;
      ecertificate.email = data.email;
      ecertificate.phone = data.phone;
      if (data.selfie_photo) {
        ecertificate.selfiePhoto = data.selfie_photo;
      }
      console.log(ecertificate.selfiePhoto);
      if (data.ktp_photo) {
        ecertificate.ktpPhoto = data.ktp_photo;
      }
      console.log('Test');
      console.log(ecertificate.ktpPhoto);
      ecertificate.channel = data.channel;
      ecertificate.referenceId = data.reference_id;
      ecertificate.biometricResponse = result;
      ecertificate.biometricStatus = status;
      this.ecertificateRepository.save(ecertificate);   

    } catch {}    
  }

  async saveOtentikasi(authorization: string, data: FaceVerificationDto, result: any, status: string) {   
    try {
      const username = this.getJWTUser(authorization);      
      if (!await this.viewPeserta(data.subject_id)) {
        let peserta = new Peserta();
        peserta.notas = data.subject_id;
        this.pesertaRepository.save(peserta);
      }

      let otentikasi = new Otentikasi();
      otentikasi.username = username;
      otentikasi.notas = data.subject_id;
      if (data.image) {
        otentikasi.photo = data.image;
      }
      otentikasi.biometricResponse = result;
      otentikasi.biometricStatus = status;
      otentikasi.latitude = data.latitude;
      otentikasi.longitude = data.longitude;      
      otentikasi.date = new Date();
      this.otentikasiRepository.save(otentikasi);

    } catch {}    
  }

  getEnrollmentById(id: number): Promise<Enrollment> {
    return this.enrollmentRepository
               .createQueryBuilder("enrollment")
               .where("enrollment.id= :id", { id: id })
               .getOne();
  }

  getOtentikasiById(id: number): Promise<Otentikasi> {  
    return this.otentikasiRepository
               .createQueryBuilder("otentikasi")
               .where("otentikasi.id= :id", { id: id })
               .getOne();
  }

  getOcrKtpById(id: number): Promise<Ocrktp>{
    return this.ocrktpRepository
               .createQueryBuilder("ocrktp")
               .where("ocrktp.id= :id", {id: id})
               .getOne();
  }

  getEcertificateVerificationById(id: number): Promise<Ecertificate>{
    return this.ecertificateRepository
               .createQueryBuilder("ecertificate_verification")
               .where("ecertificate_verification.id= :id", {id: id})
               .getOne();
  }
  
    public getPagingEnrollment(query: PaginateQuery): Promise<Paginated<Enrollment>> {
    return paginate(query, this.enrollmentRepository, {
      sortableColumns: ['id', 'username', 'notas','biometricStatus','flag','date'],
      nullSort: 'last',
      defaultSortBy: [['notas', 'ASC']],
      searchableColumns: ['id', 'username', 'notas','biometricStatus','date'],
      select: ['id', 'username', 'notas', 'photo', 'biometricResponse','biometricStatus','flag','latitude','longitude','date'],
      filterableColumns: {
        notas: [FilterOperator.EQ, FilterSuffix.NOT],
       
      },
    })
  }


  public getPagingOtentikasi(query: PaginateQuery): Promise<Paginated<Otentikasi>> {

    

    return paginate(query, this.otentikasiRepository, {
      sortableColumns: ['id', 'username', 'notas','biometricStatus','nedStatus','date'],
      nullSort: 'last',
      defaultSortBy: [['notas', 'ASC']],
      searchableColumns: ['id', 'username', 'notas','biometricStatus','date'],
      select: ['id', 'username', 'notas', 'photo', 'biometricResponse','biometricStatus','nedResponse','nedStatus','latitude','longitude','date'],
      filterableColumns: {
        notas: [FilterOperator.EQ, FilterSuffix.NOT],
       // age: true,
      },
    })
  }

}
