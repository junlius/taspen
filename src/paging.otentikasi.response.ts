import { ApiProperty } from '@nestjs/swagger';  
import { Otentikasi } from './entity/otentikasi.entity';

export class PagingOtentitasiResponse {
    @ApiProperty()
    data: Otentikasi[]
    @ApiProperty()
    success: boolean
    @ApiProperty()
    total: number
  }
  