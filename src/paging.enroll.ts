import { ApiProperty } from '@nestjs/swagger';  
import { Enrollment } from './entity/enrollment.entity';


export class Daum {
 @ApiProperty()
  id: number
  @ApiProperty()
    username: string
    @ApiProperty()
  notas: string
  biometricResponse: BiometricResponse
  biometricStatus: string
  flag: any
  latitude: string
  longitude: string
  date: string
}

export class BiometricResponse {
  nface: number
  message: string
  rotation: number
  timestamp: number
  attributes: Attributes
  session_id: string
  subject_id: string
  status_code: string
  bounding_box: BoundingBox
  face_landmark: FaceLandmark
  image_quality: ImageQuality
}

export class Attributes {
  mask_on: boolean
  veil_on: boolean
  sunglasses_on: boolean
}

export class BoundingBox {
  Width: string
  Height: string
  TopLeftX: string
  TopLeftY: string
  BottomRightX: string
  BottomRightY: string
}

export class FaceLandmark {
  NoseX: string
  NoseY: string
  LeftEyeX: string
  LeftEyeY: string
  RightEyeX: string
  RightEyeY: string
  MouthLeftX: string
  MouthLeftY: string
  MouthRightX: string
  MouthRightY: string
}

export class ImageQuality {
  blur: boolean
  dark: boolean
  grayscale: boolean
}

export class Meta {
  itemsPerPage: number
  totalItems: number
  currentPage: number
  totalPages: number
  sortBy: string[][]
}

export class Links {
  current: string
  next: string
  last: string
}

export class pagingEnrollResponse {
  data: Daum[]
  meta: Meta
  links: Links
}