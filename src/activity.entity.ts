import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'activity' })
export class Activity {
  /**
   * this decorator will help to auto generate id for the table.
   */
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 30, name: "activity_type" })
  activityType: string;

  @Column({ type: 'varchar', length: 20  })
  username: string;

  @Column({ type: 'varchar', name: "activity_desc" })
  activityDesc: string;

  @Column({ type: 'varchar', name: "http_request" })
  httpRequest: string;

  @Column({ type: 'varchar', name: "http_response" })
  httpResponse: string;

  @Column({ type: 'varchar', length: 10, name: "response_status" })
  responseStatus: string;

  @Column({ type: 'timestamptz', length: 10, name: "response_status" })
  date: Date;

  /**
   *activity_type  varchar(30),
	username varchar(20),
	activity_desc TEXT,
	http_request JSONB,
	http_response JSONB,
	response_status varchar(10),
	date timestamp  timestamptz
   */

//   @Column({ type: 'varchar', length: 20 })
//   username: string;

//   @Column({ type: 'varchar', length: 100  })
//   password: string;

//   @Column({ type: 'varchar', length: 30, name: "first_name" })
//   firstName: string;

//   @Column({ type: 'varchar', length: 30, name: "last_name" })
//   lastName: string;
  
}