import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'peserta' })
export class Peserta {
  /**
   * this decorator will help to auto generate id for the table.
   */
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 30 })
  notas: string;

  @Column({ type: 'varchar', length: 100  })
  nama: string;

  @Column({ type: 'varchar', length: 30 })
  nik: string;

  @Column({ type: 'varchar' })
  alamat: string;
  
}