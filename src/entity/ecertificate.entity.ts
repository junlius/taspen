import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'ecertificate_verification' })
export class Ecertificate {
    /**
     * this decorator will help to auto generate id for the table.
     */
    @ApiProperty()
    @PrimaryGeneratedColumn()  
    id: number;
    
    @ApiProperty()
    @Column({ type: 'varchar', length: 20  })
    username: string;

    @ApiProperty()
    @Column({ type: 'varchar', length: 100})
    name: string;

    @ApiProperty()
    @Column({ type: 'date', name: 'birth_date' })
    birthDate: string;

    @ApiProperty()
    @Column({ type: 'varchar', length: 100})
    email: string;

    @ApiProperty()
    @Column({ type: 'varchar', length: 100})
    phone: string;

    @ApiProperty()
    @Column({ type: "bytea", nullable: false, name: 'selfie_photo',
              transformer: {
                to: (value: string) => Buffer.from(value, 'base64'),
                from: (value: Buffer) => value.toString('base64')
              }
            })
    selfiePhoto: string;

    @ApiProperty()
    @Column({ type: "bytea", nullable: false, name: 'ktp_photo',
              transformer: {
                to: (value: string) => Buffer.from(value, 'base64'),
                from: (value: Buffer) => value.toString('base64')
              }
            })
    ktpPhoto: string;

    @ApiProperty()
    @Column({ type: 'varchar', length: 100})
    channel: string;

    @ApiProperty()
    @Column({ type: 'varchar', length: 20, name: 'reference_id' })
    referenceId: string;

    @ApiProperty()
    @Column({ type: 'jsonb', name: "biometric_response" })
    biometricResponse: any;

    @ApiProperty()
    @Column({ type: 'varchar', name: "biometric_status" })
    biometricStatus: string;
}