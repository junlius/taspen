import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'pmt_channel_progress' })
export class PmtChannelProgressEntity {
  /**
   * this decorator will help to auto generate id for the table.
   */
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ type: 'int', name: "otentikasi_id" })
  otentikasiId: number;

  @ApiProperty()
  @Column({ type: 'varchar', length: 15, name: "batch_code" })
  batchCode: string;

  @ApiProperty()
  @Column({ type: 'varchar', length: 5, name: "bank_code" })
  bankCode: string;

  @ApiProperty()
  @Column({ type: "timestamptz", default: () => "now()" })
  timestamp: Date;

  @ApiProperty()
  @Column({ type: 'int', name: "retry_no" })
  retryNo: number;

  @ApiProperty()
  @Column({ type: 'varchar', length: 10 })
  status: string;

}