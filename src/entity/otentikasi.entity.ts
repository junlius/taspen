import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger'; 

@Entity({ name: 'otentikasi' })
export class Otentikasi {
  /**
   * this decorator will help to auto generate id for the table.
   */
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ type: 'varchar', length: 20  })
  username: string;

  @ApiProperty()
  @Column({ type: 'varchar', length: 30  })
  notas: string;

  @ApiProperty()
  @Column({ type: "bytea", nullable: false,
            transformer: {
              to: (value: string) => Buffer.from(value, 'base64'),
              from: (value: Buffer) => value.toString('base64')
            }
          })
  photo: string;

  @ApiProperty()
  @Column({ type: 'jsonb', name: "biometric_response" })
  biometricResponse: any;

  @ApiProperty()
  @Column({ type: 'varchar', name: "biometric_status" })
  biometricStatus: string;

  @ApiProperty()
  @Column({ type: 'jsonb', name: "ned_response" })
  nedResponse: any;

  @ApiProperty()
  @Column({ type: 'varchar', name: "ned_status" })
  nedStatus: string;

  @ApiProperty()
  @Column({ type: 'varchar' })
  latitude: string;

  @ApiProperty()
  @Column({ type: 'varchar' })
  longitude: string;

  @ApiProperty()
  @Column({ type: "timestamptz", default: () => "now()" })
  date: Date;
  
}