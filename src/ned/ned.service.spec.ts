import { Test, TestingModule } from '@nestjs/testing';
import { NedService } from './ned.service';

describe('NedService', () => {
  let service: NedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NedService],
    }).compile();

    service = module.get<NedService>(NedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
