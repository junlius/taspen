import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { NedService } from './ned.service';
import { CreateNedDto } from './dto/create-ned.dto';
import { UpdateNedDto } from './dto/update-ned.dto';
import { ApiExcludeController } from '@nestjs/swagger';

@ApiExcludeController()
@Controller('ned')
export class NedController {
  constructor(private readonly nedService: NedService) {}

  @Post()
  create(@Body() createNedDto: CreateNedDto) {
    return this.nedService.create(createNedDto);
  }

  @Get()
  findAll() {
    return this.nedService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.nedService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNedDto: UpdateNedDto) {
    return this.nedService.update(+id, updateNedDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.nedService.remove(+id);
  }
}
