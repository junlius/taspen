import { PartialType } from '@nestjs/mapped-types';
import { CreateNedDto } from './create-ned.dto';

export class UpdateNedDto extends PartialType(CreateNedDto) {}
