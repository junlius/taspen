import { Test, TestingModule } from '@nestjs/testing';
import { NedController } from './ned.controller';
import { NedService } from './ned.service';

describe('NedController', () => {
  let controller: NedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NedController],
      providers: [NedService],
    }).compile();

    controller = module.get<NedController>(NedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
