import { Module } from '@nestjs/common';
import { NedService } from './ned.service';
import { NedController } from './ned.controller';

@Module({
  controllers: [NedController],
  providers: [NedService],
})
export class NedModule {}
