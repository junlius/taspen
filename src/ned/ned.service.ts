import { Injectable } from '@nestjs/common';
import { CreateNedDto } from './dto/create-ned.dto';
import { UpdateNedDto } from './dto/update-ned.dto';

@Injectable()
export class NedService {
  create(createNedDto: CreateNedDto) {
    return 'This action adds a new ned';
  }

  findAll() {
    return `This action returns all ned`;
  }

  findOne(id: number) {
    return `This action returns a #${id} ned`;
  }

  update(id: number, updateNedDto: UpdateNedDto) {
    return `This action updates a #${id} ned`;
  }

  remove(id: number) {
    return `This action removes a #${id} ned`;
  }
}
