export const jwtConstants = {
    secret: '###POC-TASPEN###',
};

export const verihubsAPI = {
    faceVerification: 'https://api.verihubs.com/v1/face/verify',
    faceEnrollment: 'https://api.verihubs.com/v1/face/enroll',
    ktpExtract: 'https://api.verihubs.com/ktp/id/extract-async',
    ktpExtractResult: 'https://api.verihubs.com/ktp/id/extract-async/result',
    electronicCertificateVerification: 'https://api.verihubs.com/data-verification/certificate-electronic/verify',
    appID: 'e79a654d-2c83-4899-8415-f779ff41e632',
    apiKey: 'SPV/BUwzRDsoq2qHx+hrn6hm2BqNGUwP'
}

export const defaultSettings = {
    verihubsKtpExtractResultWaitingTime: 5000,
}

export const activityType = {    
    faceEnroll: 'FACE_ENROLL',
    faceVerify: 'FACE_VERIFy',
    ktpExtract: 'KTP_EXTRACT',
    ecertificateVerify: 'E_CERTIFICATE_VERIFY'
}

export const status = {
    success: 'SUCCESS',
    failed: 'FAILED'    
}