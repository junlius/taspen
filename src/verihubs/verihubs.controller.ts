import { Controller, Get, Post, Request, Body, Patch, Param, Delete, UseGuards, Logger } from '@nestjs/common';
import { VerihubsService } from './verihubs.service';
import { AuthGuard } from '../auth.guard';
import { FaceVerificationDto } from './dto/face.verification.dto';
import { FaceEnrollmentDto } from './dto/face.enrollment.dto';
import { ElectronicCertificateVerificationDto } from './dto/ecertificate.verification.dto';
import { KtpExtractDto } from './dto/ktp.extract.dto';
import { ApiTags, ApiCreatedResponse, ApiExcludeEndpoint, ApiHeader, ApiOkResponse } from '@nestjs/swagger';
import { FaceEnrollmentResponse } from './dto/face.enrollment.response';
import { FaceVerificationResponse } from './dto/face.verification.response';
import { ElectronicCertificateVerificationResponse } from './dto/ecertificate.verification.response';
import { KtpExtractResult } from './dto/ktp.extract.response';
import { AppService } from 'src/app.service';
import { request } from 'http';
import { constants } from 'buffer';
import { status } from '../constants';
import { AdjudicationSearchRequest, AdjudicationSearchResponse } from './dto/manual.adjudication.dto';

@Controller('api')
export class VerihubsController {
  private readonly logger = new Logger();
  constructor(private readonly verihubsService: VerihubsService,
               ) {}

  @ApiTags('Face Enrollment')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Face enrollment succeeded.',
    type: FaceEnrollmentResponse
  })
  @UseGuards(AuthGuard)
  @Post('face/enroll')
  async faceEnrollment(@Request() request, @Body() faceEnrollmentDto: FaceEnrollmentDto) {         
      return await this.verihubsService.faceEnrollment(request.headers.authorization, faceEnrollmentDto);
  }

  @ApiTags('Face Verification')  
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Face verification succeeded.',
    type: FaceVerificationResponse
  })
  @UseGuards(AuthGuard)
  @Post('face/verify')
  async faceVerification(@Request() request, @Body() faceVerificationDto: FaceVerificationDto) {
      return await this.verihubsService.faceVerification(request.headers.authorization, faceVerificationDto);
  }

  @ApiTags('KTP Extraction')  
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'KTP extraction succeeded.',
    type: KtpExtractResult
  })
  @UseGuards(AuthGuard)
  @Post('ktp/extract')
  async ktpExtract(@Request() request, @Body() ktpExtractDto: KtpExtractDto) {
      return await this.verihubsService.ktpExtract(request.headers.authorization, ktpExtractDto);
  }

  @ApiTags('Electronic Certificate Verification')  
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Electronic certificate verification succeeded.',
    type: ElectronicCertificateVerificationResponse
  })
  @UseGuards(AuthGuard)
  @Post('ecertificate/verify')
  async ecertificateVerification(@Request() request, @Body() electronicCertificateVerificationDto: ElectronicCertificateVerificationDto) {
      return await this.verihubsService.ecertificateVerification(request.headers.authorization, electronicCertificateVerificationDto);
  }
  
  @ApiTags('Manual Adjucation Search')  
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Manual Adjucation Search succeeded.',
    type: AdjudicationSearchResponse
  })
  // @UseGuards(AuthGuard)
  @Post('adjucation/search')
  async adjudicationSearch(@Body() AdjudicationSearchRequest : AdjudicationSearchRequest) {
    // You can perform any processing or validation here if needed
    return AdjudicationSearchRequest;
  }
}
