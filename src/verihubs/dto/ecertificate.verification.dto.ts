import { ApiProperty } from '@nestjs/swagger';
export class ElectronicCertificateVerificationDto {
    @ApiProperty()
    nik: string;
    @ApiProperty()
    name: string;
    @ApiProperty()
    birth_date: string;
    @ApiProperty()
    email: string;
    @ApiProperty()
    phone: string;
    @ApiProperty()
    selfie_photo: string;
    @ApiProperty()
    ktp_photo: string;
    @ApiProperty()
    channel: string;
    @ApiProperty()
    reference_id: string;
}