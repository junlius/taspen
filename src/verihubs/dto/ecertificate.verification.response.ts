import { ApiProperty } from '@nestjs/swagger'; 

  export class RejectField {}  
  export class Data {
    @ApiProperty()
    id: string
    @ApiProperty()
    status: string
    @ApiProperty()
    reject_field: RejectField
    @ApiProperty()
    reference_id: string
  }
  
  export class ElectronicCertificateVerificationResponse {
    @ApiProperty()
    message: string
    @ApiProperty()
    data: Data
  }