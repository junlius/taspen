import { ApiProperty } from '@nestjs/swagger';  

export class Attributes {
    @ApiProperty()
    sunglasses_on: boolean
    @ApiProperty()
    mask_on: boolean
    @ApiProperty()
    veil_on: boolean
}

export class Liveness {
    @ApiProperty()
    status: boolean
    @ApiProperty()
    probability: string    
}

export class ImageQuality {
    @ApiProperty()
    blur: boolean
    @ApiProperty()
    dark: boolean
    @ApiProperty()
    grayscale: boolean
}

export class BoundingBox {
    @ApiProperty()
    TopLeftX: string
    @ApiProperty()
    TopLeftY: string
    @ApiProperty()
    BottomRightX: string
    @ApiProperty()
    BottomRightY: string
    @ApiProperty()
    Width: string
    @ApiProperty()
    Height: string
}

export class FaceLandmark {
    @ApiProperty()
    LeftEyeX: string
    @ApiProperty()
    LeftEyeY: string
    @ApiProperty()
    RightEyeX: string
    @ApiProperty()
    RightEyeY: string
    @ApiProperty()
    NoseX: string
    @ApiProperty()
    NoseY: string
    @ApiProperty()
    MouthLeftX: string
    @ApiProperty()
    MouthLeftY: string
    @ApiProperty()
    MouthRightX: string
    @ApiProperty()
    MouthRightY: string
}
          
export class FaceVerificationResponse {
    @ApiProperty()
    status_code: string
    @ApiProperty()
    message: string
    @ApiProperty()
    subject_id: string
    @ApiProperty()
    session_id: string
    @ApiProperty()
    attributes: Attributes
    @ApiProperty()
    liveness: Liveness
    @ApiProperty()
    image_quality: ImageQuality
    @ApiProperty()
    rotation: number
    @ApiProperty()
    bounding_box: BoundingBox
    @ApiProperty()
    face_landmark: FaceLandmark
    @ApiProperty()
    nface: number
    @ApiProperty()
    similarity_status: boolean
    @ApiProperty()
    timestamp: number
}