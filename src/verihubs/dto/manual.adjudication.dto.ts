import { ApiProperty } from '@nestjs/swagger';

// Create a DTO (Data Transfer Object) for the request body
export class AdjudicationSearchRequest {
  @ApiProperty()
  nama: string;
  @ApiProperty()
  nik: string;
  @ApiProperty()
  notas: string;
}

// Create a DTO for the response body
export class AdjudicationSearchResponse {
  @ApiProperty()
  nama: string;
  @ApiProperty()
  nik: string;
  @ApiProperty()
  notas: string;
}