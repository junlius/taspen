import { ApiProperty } from '@nestjs/swagger';
export class KtpExtractDto {
    @ApiProperty()
    image: string;
    @ApiProperty()
    validate_quality: boolean;
    @ApiProperty()
    reference_id: string;
}