import { ApiProperty } from '@nestjs/swagger';

export class ImageQuality {
    @ApiProperty()
    blur: boolean
    @ApiProperty()
    dark: boolean
    @ApiProperty()
    flashlight: boolean
    @ApiProperty()
    grayscale: boolean
  }
  
  export class Data {
    @ApiProperty()
    address: string
    @ApiProperty()
    administrative_village: string
    @ApiProperty()
    blood_type: string
    @ApiProperty()
    city: string
    @ApiProperty()
    date_of_birth: string
    @ApiProperty()
    district: string
    @ApiProperty()
    full_name: string
    @ApiProperty()
    gender: string
    @ApiProperty()
    marital_status: string
    @ApiProperty()
    nationality: string
    @ApiProperty()
    nik: string
    @ApiProperty()
    occupation: string
    @ApiProperty()
    place_of_birth: string
    @ApiProperty()
    religion: string
    @ApiProperty()
    rt_rw: string
    @ApiProperty()
    state: string
    @ApiProperty()
    image_quality: ImageQuality
  }

  export class KtpExtractResult {
    @ApiProperty()
    message: string
    @ApiProperty()
    error_code: string
    @ApiProperty()
    data: Data
  }
  