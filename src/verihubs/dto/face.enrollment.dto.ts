import { ApiProperty } from '@nestjs/swagger';
export class FaceEnrollmentDto {
    @ApiProperty()
    image: string;

    @ApiProperty()
    subject_id: string;

    @ApiProperty()
    is_quality: boolean;

    @ApiProperty()
    is_attribute: boolean;

    @ApiProperty()
    is_liveness: boolean;

    @ApiProperty()
    validate_quality: boolean;

    @ApiProperty()
    validate_attribute: boolean;

    @ApiProperty()
    validate_liveness: boolean;

    @ApiProperty()
    validate_nface: boolean;

    @ApiProperty()
    latitude: string;

    @ApiProperty()
    longitude: string;
  }