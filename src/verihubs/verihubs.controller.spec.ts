import { Test, TestingModule } from '@nestjs/testing';
import { VerihubsController } from './verihubs.controller';
import { VerihubsService } from './verihubs.service';

describe('VerihubsController', () => {
  let controller: VerihubsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VerihubsController],
      providers: [VerihubsService],
    }).compile();

    controller = module.get<VerihubsController>(VerihubsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
