import { Module, forwardRef } from '@nestjs/common';
import { VerihubsService } from './verihubs.service';
import { VerihubsController } from './verihubs.controller';
import { HttpModule } from '@nestjs/axios';
import { AppModule } from 'src/app.module';

@Module({
  imports: [HttpModule, forwardRef(() => AppModule)],
  controllers: [VerihubsController],
  providers: [VerihubsService],
})
export class VerihubsModule {}
