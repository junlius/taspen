import { Test, TestingModule } from '@nestjs/testing';
import { VerihubsService } from './verihubs.service';

describe('VerihubsService', () => {
  let service: VerihubsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VerihubsService],
    }).compile();

    service = module.get<VerihubsService>(VerihubsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
