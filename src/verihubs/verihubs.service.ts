import { Injectable, BadRequestException, Logger } from '@nestjs/common';
import { verihubsAPI, defaultSettings } from '../constants';
import { FaceVerificationDto } from './dto/face.verification.dto';
import { FaceEnrollmentDto } from './dto/face.enrollment.dto';
import { KtpExtractDto } from './dto/ktp.extract.dto';
import { ElectronicCertificateVerificationDto } from './dto/ecertificate.verification.dto';
import { HttpService } from '@nestjs/axios';
import { AxiosRequestConfig, AxiosError } from "axios";
import { catchError, firstValueFrom } from 'rxjs';
import { AppService } from 'src/app.service';
import { status } from '../constants';


@Injectable()
export class VerihubsService {
  private readonly logger = new Logger(VerihubsService.name);

  constructor(private readonly httpService: HttpService,
              private readonly appService: AppService) {}

  async faceVerification(authorization: string, faceVerificationDto: FaceVerificationDto) {
    const headersRequest = {
      'Content-Type': 'application/json',
      'App-ID': verihubsAPI.appID,
      'API-Key': verihubsAPI.apiKey
    };

    let response;
    try {
      let verihubsData = Object.assign({}, faceVerificationDto);
      delete verihubsData['latitude'];
      delete verihubsData['longitude'];  

      response = await firstValueFrom(
        this.httpService.post(verihubsAPI.faceVerification, verihubsData, {headers: headersRequest}).pipe(
          catchError((error: AxiosError) => {               
            //console.log(error.response.data);                
            throw new BadRequestException(error.response.data);
          }),
        ),
      );
    } catch (error) {    
      this.appService.saveOtentikasi(authorization, faceVerificationDto, error.response, status.failed);
      throw new BadRequestException(error.response.message, { cause: new Error(), description: error.response.error_code })
    } 

    this.appService.saveOtentikasi(authorization, faceVerificationDto, response.data, status.success);
    return response.data;
  }

  async faceEnrollment(authorization: string, faceEnrollmentDto: FaceEnrollmentDto) {
    const headersRequest = {
      'Content-Type': 'application/json',
      'App-ID': verihubsAPI.appID,
      'API-Key': verihubsAPI.apiKey
    };

    let response;
    try {
      let verihubsData = Object.assign({}, faceEnrollmentDto);
      delete verihubsData['latitude'];
      delete verihubsData['longitude'];      
      
      response = await firstValueFrom(
        this.httpService.post(verihubsAPI.faceEnrollment, verihubsData, {headers: headersRequest}).pipe(
          catchError((error: AxiosError) => {               
            //console.log(error.response.data);                 
            throw new BadRequestException(error.response.data);
          }),
        ),
      );
    } catch (error) {    
      this.appService.saveEnrollment(authorization, faceEnrollmentDto, error.response, status.failed);
      throw new BadRequestException(error.response.message, { cause: new Error(), description: error.response.error_code })
    } 

    this.appService.saveEnrollment(authorization, faceEnrollmentDto, response.data, status.success);
    return response.data;
  }

  async ktpExtract(authorization: string, ktpExtractDto: KtpExtractDto) {
    const headersRequest = {
      'Content-Type': 'application/json',
      'App-ID': verihubsAPI.appID,
      'API-Key': verihubsAPI.apiKey
    };

    let response;
    try {
      // extract
      response = await firstValueFrom(
        this.httpService.post(verihubsAPI.ktpExtract, ktpExtractDto, {headers: headersRequest}).pipe(
          catchError((error: AxiosError) => {                 
            throw new BadRequestException(error.response.data);
          }),
        ),
      );      
      
      await this.wait(defaultSettings.verihubsKtpExtractResultWaitingTime);

      // get result
      if (response) {
        const params = {reference_id: ktpExtractDto.reference_id};
        response = await firstValueFrom(                    
          this.httpService.get(verihubsAPI.ktpExtractResult, {headers: headersRequest, params: params}).pipe(
            catchError((error: AxiosError) => {                 
              throw new BadRequestException(error.response.data);
            }),
          ),
        );        
      }
      
    } catch (error) {
      this.appService.saveOcrktp(authorization,ktpExtractDto,error.response,status.failed)  
      throw new BadRequestException(error.response.message, { cause: new Error(), description: error.response.error_code })
    } 

    this.appService.saveOcrktp(authorization,ktpExtractDto,response.data,status.success);
    return response.data;
  }


  async ecertificateVerification(authorization: string, electronicCertificateVerificationDto: ElectronicCertificateVerificationDto) {
    const headersRequest = {
      'Content-Type': 'application/json',
      'App-ID': verihubsAPI.appID,
      'API-Key': verihubsAPI.apiKey
    };

    let response;
    try {
      response = await firstValueFrom(
        this.httpService.post(verihubsAPI.electronicCertificateVerification, electronicCertificateVerificationDto, {headers: headersRequest}).pipe(
          catchError((error: AxiosError) => {                     
            throw new BadRequestException(error.response.data);
          }),
        ),
      );
    } catch (error) { 
      this.appService.saveEcertificateVerification(authorization,electronicCertificateVerificationDto,error.response,status.failed)   
      throw new BadRequestException(error.response.message, { cause: new Error(), description: error.response.error_code })
    } 

    this.appService.saveEcertificateVerification(authorization,electronicCertificateVerificationDto,response.data,status.success);
    return response.data;
  }


  wait(time) {
    return new Promise(resolve => {
        setTimeout(resolve, time);
    });
  }


  saveEnrollment(authorization: string, data: FaceEnrollmentDto, result: any, status: string) {
    this.appService.saveEnrollment(authorization, data, result, status);
  }
}
