import { ApiProperty } from '@nestjs/swagger'; 
  
  export class Paging {
    @ApiProperty()
    current: number
    @ApiProperty()
    pageSize: number
  }
  
  export class Sort {
    @ApiProperty()
    fieldName: string
  }

  export class FilterKeyValue {
    @ApiProperty()
    fieldName: string
  }
  
  export class Filter {
    @ApiProperty()
    fieldName: string
  }

  export class PagingRequest {
    @ApiProperty()
    paging: Paging
    @ApiProperty()
    sort: Sort
    @ApiProperty()
    filter: Filter[]
  }
  