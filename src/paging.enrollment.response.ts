import { ApiProperty } from '@nestjs/swagger';  
import { Enrollment } from './entity/enrollment.entity';

export class PagingEnrollmentResponse {
    @ApiProperty()
    data: Enrollment[]
    @ApiProperty()
    success: boolean
    @ApiProperty()
    total: number
  }
  