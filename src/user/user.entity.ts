import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User {
  /**
   * this decorator will help to auto generate id for the table.
   */
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 20 })
  username: string;

  @Column({ type: 'varchar', length: 100, })
  password: string;

  @Column({ type: 'varchar', length: 30, name: "first_name" })
  firstName: string;

  @Column({ type: 'varchar', length: 30, name: "last_name" })
  lastName: string;
  
}