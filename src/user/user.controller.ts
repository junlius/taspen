import { Injectable, Body, Controller, Post, Request, Get, UseGuards, Res, HttpStatus, UnauthorizedException } from '@nestjs/common';
import {UserLoginDto, UserToken} from './user.dto';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '../auth.guard';
import { UserService } from './user.service';
import { ApiTags, ApiCreatedResponse, ApiExcludeEndpoint, ApiOkResponse } from '@nestjs/swagger';

@Injectable()
@Controller('api')
export class UserController {
    constructor(
        private jwtService: JwtService,
        private userService: UserService
      ) {}

    @ApiTags('Authentication')  
    @ApiOkResponse({
        description: 'Authentication succeeded.',
        type: UserToken,
      })
    @Post('authenticate')
     async authenticate(@Body() userLoginDTO: UserLoginDto): Promise<UserToken> {
        const payload = { sub: userLoginDTO.username };
        const user = await this.userService.viewUser(userLoginDTO.username);
        if (!user) {
            throw new UnauthorizedException();
        }
        
        try {
            const hash = user.password;
            const isMatch = await this.userService.compare(userLoginDTO.password, hash);
            if (!isMatch) {
                throw new UnauthorizedException();
            }
            return new UserToken(await this.jwtService.sign(payload));
        } catch (error) {
            throw new UnauthorizedException();
        }
    }

    @ApiExcludeEndpoint()
    @UseGuards(AuthGuard)
    @Get('account')
    async getAccount() {
        const hash = await this.userService.hash('password');
        const isMatch = await this.userService.compare("password", hash);
        //console.log(isMatch);
        return hash;
    }

    @ApiExcludeEndpoint()
    @UseGuards(AuthGuard)
    @Get('user')
    findAll() {
        return this.userService.findAllUser();
     }
}
