import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
      ) {}

    async hash(password) {
        const saltOrRounds = 10;
        return await bcrypt.hash(password, saltOrRounds);
    }

    async compare(password, hash) {
        return await bcrypt.compare(password, hash);
    }

    findAllUser(): Promise<User[]> {
        return this.userRepository.find();
    }

    viewUser(username: string): Promise<User> {
        return this.userRepository.findOneBy({ username });
    }

    findUserPagination(): Promise<User[]> {        
        return this.userRepository.find();
    }
}
