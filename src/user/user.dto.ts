import { ApiProperty } from '@nestjs/swagger';

export class UserLoginDto {
    @ApiProperty()
    username: string;

    @ApiProperty()
    password: string;
  }

export class UserToken {
    @ApiProperty()
    token: string;
    constructor(token: string) {
      this.token = token;
    }
}