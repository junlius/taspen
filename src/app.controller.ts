import { Controller, Get, Request, Param, Body, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { get } from 'http';
import { Peserta } from './entity/peserta.entity';
import { Enrollment } from './entity/enrollment.entity';
import { ApiTags, ApiCreatedResponse, ApiExcludeEndpoint, ApiHeader, ApiOkResponse, ApiQuery } from '@nestjs/swagger';
import { PagingRequest } from './paging.dto';
import { PagingOtentitasiResponse } from './paging.otentikasi.response';
import { Ocrktp } from './entity/ocrktp.entity';
import { Ecertificate } from './entity/ecertificate.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { pagingEnrollResponse } from './paging.enroll';
import { Otentikasi } from './entity/otentikasi.entity';
import { FilterOperator, FilterSuffix, Paginate, PaginateQuery, paginate, Paginated } from 'nestjs-paginate';


@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService, @InjectRepository(Enrollment) private readonly enrollmentRepository: Repository<Enrollment>,) {}

  @ApiTags('Get enrollment data by id')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get enrollment succeeded.',
    type: Enrollment
  })
  @Get('enrollment/:id')
  getEnrollment(@Param('id') id: number) {
    return this.appService.getEnrollmentById(id);
  }

  @ApiTags('Get otentikasi data by id')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get otentikasi succeeded.',
    type: Enrollment
  })
  @Get('otentikasi/:id')
  getOtentikasi(@Param('id') id: number) {
    return this.appService.getOtentikasiById(id);
  }

  @ApiTags('Get Ecertifaicate Verification data by id')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get Ecertifaicate Verification succeeded.',
    type: Ecertificate
  })
  @Get('ecertificate/:id')
  getEcertificateVerificationById(@Param('id') id: number) {
    return this.appService.getEcertificateVerificationById(id);
  }

  @ApiTags('Get Ocr KTP data by id')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get Ocr KTP succeeded.',
    type: Ocrktp
  })
  @Get('ocrktp/:id')
  getOcrKtpById(@Param('id') id: number) {
    return this.appService.getOcrKtpById(id);
  }


  @ApiTags('Get enrollment paging data')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get enrollment paging succeeded.',
    type: Paginated
  })
  

  @Post('enrollment')
    findAllEnrolment( @Body() query: PaginateQuery ): Promise<Paginated<Enrollment>> {
    return this.appService.getPagingEnrollment(query);
    
  }


  @ApiTags('Get otentikasi paging data')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get otentikasi paging succeeded.',
    type: Paginated
  })
  @Post('otentikasi')
  findAllOtentikasi(@Body() query: PaginateQuery): Promise<Paginated<Otentikasi>> {
    return this.appService.getPagingOtentikasi(query);


  }
  
  
}
