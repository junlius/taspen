import * as winston from 'winston';

const { splat, combine, timestamp, printf, colorize } = winston.format;

// meta param is ensured by splat()
const myFormat = printf(({ timestamp, level, message, meta }) => {
  // return `${timestamp};${level};${message};${meta ? JSON.stringify(meta) : ''}`;
  return `${timestamp} [${level}] : ${JSON.stringify(message)} ${meta ? '|' + JSON.stringify(meta) : ''}`;
});

const logger = winston.createLogger({
  level: 'info', // Set logging level
  // format: winston.format.json(), // Output format
  format: combine(
    colorize(),
    timestamp(),
    splat(),
    myFormat
  ),
  transports: [
    new winston.transports.Console(), // Log to console
    new winston.transports.File({ filename: 'error.log', level: 'error' }), // Log errors to file
  ],
});

export default logger;
