import { ApiProperty } from '@nestjs/swagger';

export class PaymentChannelRequestDto {
  @ApiProperty()
  otentikasiId: number;

  constructor(otentikasiId: number) {
    this.otentikasiId = otentikasiId;
  }
}
export class PaymentChannelQueueDto {
  @ApiProperty()
  otentikasiId: number;
  @ApiProperty()
  batchCode: string;
  @ApiProperty()
  bankCode: string;
  @ApiProperty()
  totalChannel: number;

  constructor(otentikasiId: number, batchCode: string, bankCode: string, totalChannel: number) {
    this.otentikasiId = otentikasiId;
    this.batchCode = batchCode;
    this.bankCode = bankCode;
    this.totalChannel = totalChannel;
  }
}

export class PaymentChannelResponseDto {
  @ApiProperty()
  response: boolean;

  constructor(response: boolean) {
    this.response = response;
  }
}

export class PmtProgressResponseDto {
  @ApiProperty()
  batchCode: string;
  @ApiProperty()
  totalChannel: number;
  @ApiProperty()
  totalFailed: number;
  @ApiProperty()
  nedStatus: string;
}

export class PmtProgressHistoryDto {
  @ApiProperty()
  otentikasiId: string;
  @ApiProperty()
  batchCode: string;
  @ApiProperty()
  totalChannel: number;
  @ApiProperty()
  totalFailed: number;
  @ApiProperty()
  status: string;
  @ApiProperty()
  progressDate: Date;
}