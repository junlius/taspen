import { Injectable, Body, Controller, Post, Request, Get, UseGuards, Res, HttpStatus, UnauthorizedException, Param } from '@nestjs/common';
import { ApiTags, ApiCreatedResponse, ApiExcludeEndpoint, ApiHeader, ApiOkResponse } from '@nestjs/swagger';
import { SimulationService } from './simulation.service';
import { PmtProgressHistoryDto } from './simulation.dto';

@Injectable()
@Controller('api')
export class SimulationController {
  constructor(
    private simulationService: SimulationService
  ) { }

  @ApiTags('Simulasi')
  @Post('paymentConnect')
  paymentConnect() {
    return this.simulationService.simulateOpenPaymentChannelOld();
  }

  @ApiTags('Simulasi')
  @Post('trigger-process/:id')
  async triggerProcess(@Param('id') id: number): Promise<string> {
    try {
      await this.simulationService.processBankConnections(id);
      return 'Process triggered successfully';
    } catch (error) {
      console.error(error);
      throw new Error('Internal Server Error');
    }
  }

  @ApiTags('Simulasi')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Get progress history list succeeded.',
    type: PmtProgressHistoryDto
  })
  @Post('trigger-process/history/:id')
  findAllProgressHistoryByOtentikasiId(@Param('id') id: number): Promise<PmtProgressHistoryDto[]> {
    return this.simulationService.getPmtProgressHistoryList(id);
  }

  @ApiTags('Simulasi')
  @ApiHeader({
    name: 'Authorization',
    description: 'Bearer token',
  })
  @ApiOkResponse({
    description: 'Check status progress buka blokir succeeded.',
    type: PmtProgressHistoryDto
  })
  @Post('trigger-process/check/:id')
  checkProgressStatusByOtentikasiId(@Param('id') id: number): Promise<PmtProgressHistoryDto[]> {
    return this.simulationService.checkPmtProgressStatus(id);
  }
}
