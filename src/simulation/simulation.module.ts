import { Module } from '@nestjs/common';
import { SimulationService } from './simulation.service';
import { SimulationController } from './simulation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PmtChannelProgressEntity } from 'src/entity/pmtChannelProgress.entity';
import { Otentikasi } from 'src/entity/otentikasi.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PmtChannelProgressEntity, Otentikasi])],
  controllers: [SimulationController],
  providers: [SimulationService],
})
export class SimulationModule { }