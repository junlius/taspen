import { Injectable } from '@nestjs/common';
import { PaymentChannelResponseDto, PaymentChannelQueueDto, PmtProgressResponseDto, PmtProgressHistoryDto } from './simulation.dto';
import express, { Request, Response } from 'express';
import { Kafka, EachMessagePayload } from 'kafkajs';
import logger from '../logger';
import { PmtChannelProgressEntity } from 'src/entity/pmtChannelProgress.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Otentikasi } from 'src/entity/otentikasi.entity';
import { Query } from 'typeorm/driver/Query';

@Injectable()
export class SimulationService {
  private readonly kafka: Kafka;
  private readonly producer;
  private readonly consumer;
  private readonly paymentChannelList: string[];
  private readonly logger;
  private readonly createQueryBuilder;

  constructor(
    @InjectRepository(PmtChannelProgressEntity) private readonly pmtChannelProgressRepo: Repository<PmtChannelProgressEntity>,
    @InjectRepository(Otentikasi) private readonly otentikasiRepository: Repository<Otentikasi>) {
    this.kafka = new Kafka({
      clientId: 'taspen-app',
      brokers: ['10.10.105.249:10092'], // Update with your Kafka broker(s)
    });

    this.producer = this.kafka.producer();
    this.consumer = this.kafka.consumer({ groupId: 'taspen-group' });

    this.paymentChannelList = ["002", "014", "008", "009", "200", "451", "022", "147", "013", "011", "016", "426", "777", "888", "999"];

    this.logger = logger;

    this.setupBankConnectionsConsumer();
  }

  simulateOpenPaymentChannelOld() {
    // Generate a random number between 0 and 1
    const randomValue = Math.random();

    // Set the probability threshold for success (adjust as needed)
    const successThreshold = 0.95;

    // Simulate opening a payment channel
    const isSuccess = randomValue <= successThreshold;

    return new PaymentChannelResponseDto(isSuccess);
  }

  private async simulateOpenPaymentChannel(bankCode: string): Promise<boolean> {
    // Generate a random number between 0 and 1
    const randomValue = Math.random();

    // Set the probability threshold for success (adjust as needed)
    const successThreshold = 0.80;

    // Simulate opening a payment channel
    const isSuccess = randomValue <= successThreshold;

    return isSuccess;
  }

  private async setupBankConnectionsConsumer(): Promise<void> {
    await this.producer.connect();
    await this.consumer.connect();

    await this.consumer.subscribe({ topic: 'bank-connections' });

    await this.consumer.run({
      eachMessage: async ({ message }: EachMessagePayload) => {
        const messageDto: PaymentChannelQueueDto = JSON.parse(message.value.toString());

        if (this.paymentChannelList.includes(messageDto.bankCode)) {
          let isSuccess = false;
          let retryCount = 0;
          while (!isSuccess) {
            isSuccess = await this.simulateOpenPaymentChannel(messageDto.bankCode);

            if (isSuccess) {
              await this.saveEnrollment(messageDto.otentikasiId, messageDto.batchCode, messageDto.bankCode, retryCount, 'SUCCESS');
              this.logger.info(`Koneksi bank ${messageDto.bankCode} berhasil`);
            } else {
              retryCount++;
              this.logger.error(`Retry ${retryCount} logic for bank ${messageDto.bankCode}`);
              if (retryCount >= 3) {
                await this.saveEnrollment(messageDto.otentikasiId, messageDto.batchCode, messageDto.bankCode, retryCount, 'FAILED');
                this.logger.info(`Koneksi bank ${messageDto.bankCode} gagal, setelah percobaan ${retryCount} kali`);
                break;
              }
            }

            const nedStatus = await this.checkPmtChannelProgress(messageDto.otentikasiId, messageDto.batchCode, messageDto.totalChannel);

            // Add a delay before the next retry (e.g., 1000 milliseconds or 1 second)
            const randomDelay = Math.random() * 1000;
            await new Promise(resolve => setTimeout(resolve, randomDelay));
          }
        }
      },
    });
  }

  generateBatchCode(): string {
    const now = new Date()
    const batchCode = now.toISOString().replace(/[-T:]/g, '').slice(0, 14);
    return batchCode
  }

  async processBankConnections(otentikasiId: number): Promise<void> {
    const batchCode = this.generateBatchCode();
    const totalChannel = this.paymentChannelList.length;
    for (const bankCode of this.paymentChannelList) {
      const queueDto = new PaymentChannelQueueDto(otentikasiId, batchCode, bankCode, totalChannel);
      await this.sendMessage('bank-connections', JSON.stringify(queueDto));
    }
  }

  private async sendMessage(topic: string, message: string): Promise<void> {
    await this.producer.send({
      topic,
      messages: [{ value: message }],
    });
  }

  async saveEnrollment(otentikasiId: number, batchCode: string, bankCode: string, retryNo: number, progressStatus: string) {
    try {
      let progressEntity = new PmtChannelProgressEntity();
      progressEntity.otentikasiId = otentikasiId;
      progressEntity.batchCode = batchCode;
      progressEntity.bankCode = bankCode;
      progressEntity.retryNo = retryNo;
      progressEntity.status = progressStatus;
      progressEntity.timestamp = new Date();
      this.pmtChannelProgressRepo.save(progressEntity);

    } catch { }
  }

  async checkPmtChannelProgress(otentikasiId: number, batchCode: string, totalChannel: number) {
    const query = `
      SELECT
        COUNT(*) AS count,
        COALESCE(SUM(CASE WHEN status = 'FAILED' THEN 1 ELSE 0 END), 0) AS hasFailed
      FROM
        pmt_channel_progress
      WHERE
        otentikasi_id = $1 AND batch_code = $2
    `;

    const result = await this.pmtChannelProgressRepo.query(query, [otentikasiId, batchCode]);

    let count = parseInt(result[0].count);
    count += 1;
    const totalFailed = parseInt(result[0].hasfailed);
    const hasFailed = totalFailed > 0;
    this.logger.info(`checkPmtChannelProgress | number: ${otentikasiId}, batchCode ${batchCode}, totalChannel ${totalChannel}`);
    this.logger.info(`checkPmtChannelProgress | count: ${count}, totalFailed: ${totalFailed}, hasFailed: ${hasFailed}`);

    let response = new PmtProgressResponseDto();
    response.batchCode = batchCode;
    response.totalChannel = totalChannel;
    response.totalFailed = totalFailed;

    if (count < totalChannel) {
      response.nedStatus = 'INPROGRESS';
      await this.setOtentikasiNedStatus(otentikasiId, response);
    } else {
      if (hasFailed) {
        response.nedStatus = 'FAILED';
        await this.setOtentikasiNedStatus(otentikasiId, response);
      } else {
        response.nedStatus = 'SUCCESS';
        await this.setOtentikasiNedStatus(otentikasiId, response);
      }
    }
  }

  async setOtentikasiNedStatus(id: number, response: PmtProgressResponseDto) {
    try {

      let otentikasi = await this.otentikasiRepository.findOneBy({ id });

      if (otentikasi.nedStatus === response.nedStatus) {
        return null;
      }

      if (!otentikasi) {
        return null; // If entity not found, return null or throw an error as per your requirement
      }

      (await otentikasi).nedStatus = response.nedStatus;
      (await otentikasi).nedResponse = response;
      const newEntity: Otentikasi = (await otentikasi);

      const updatedEntity = await this.otentikasiRepository.save(newEntity);

      return updatedEntity;

    } catch { }
  }

  async getPmtProgressHistoryList(otentikasiId: number): Promise<PmtProgressHistoryDto[]> {
    try {
      const query = `
    select
      *,
      case
        when total_failed > 0 then 'FAILED'
        else 'SUCCESS'
      end as status
    from
      (
      select
        otentikasi_id,
        batch_code,
        COUNT(1) as total_channel,
        coalesce(SUM(case when status = 'FAILED' then 1 else 0 end),0) as total_failed,
        CAST(MAX("timestamp") as date) as progress_date	
      from
        pmt_channel_progress pcp
      where
        otentikasi_id = $1
      group by
        otentikasi_id,
        batch_code)a;
    `;

      const result = await this.pmtChannelProgressRepo.query(query, [otentikasiId]);
      const parsedResult = result.map(row => ({
        otentikasiId: row.otentikasi_id,
        batchCode: row.batch_code,
        totalChannel: row.total_channel,
        totalFailed: row.total_failed,
        status: row.status,
        progressDate: row.progress_date,
      }));

      return parsedResult;

    } catch { }
  }

  async checkPmtProgressStatus(id: number): Promise<PmtProgressHistoryDto[]> {
    try {

      const otentikasi = await this.otentikasiRepository.findOneBy({ id });
      const nedResponse: PmtProgressResponseDto = otentikasi.nedResponse;
      const batchCode = nedResponse.batchCode;

      const query = `
        select
          *,
          case
            when total_failed > 0 then 'FAILED'
            else 'SUCCESS'
          end as status
        from
          (
          select
            otentikasi_id,
            batch_code,
            COUNT(1) as total_channel,
            coalesce(SUM(case when status = 'FAILED' then 1 else 0 end),0) as total_failed,
            CAST(MAX("timestamp") as date) as progress_date	
          from
            pmt_channel_progress pcp
          where
            otentikasi_id = $1 and batch_code = $2
          group by
            otentikasi_id,
            batch_code)a;
      `;

      const result = await this.pmtChannelProgressRepo.query(query, [id, batchCode]);
      const parsedResult = result.map(row => ({
        otentikasiId: row.otentikasi_id,
        batchCode: row.batch_code,
        totalChannel: row.total_channel,
        totalFailed: row.total_failed,
        status: row.status,
        progressDate: row.progress_date,
      }));

      return parsedResult;

    } catch { }
  }

}
